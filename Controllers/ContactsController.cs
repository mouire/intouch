﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using InTouch.Helpers;
using InTouch.Models;
using System.IO;
namespace InTouch.Controllers
{
    [Produces("application/json")]
    [Route("api/Contacts")]
    public class ContactsController : Controller
    {
        private readonly DataContext _context;

        public ContactsController(DataContext context)
        {
            _context = context;
        }

        // GET: api/contacts
        [HttpGet]
        public async Task<IActionResult> GetContacts([FromQuery] int page, int size, string search, string sort, string filter)
        {
            var contacts = from s in _context.Contacts
                           select s;

            if (search != null)
            {
                page = 1;
            }
            if (!String.IsNullOrEmpty(search))
            {
                contacts = contacts.Where(s => s.FirstName.Contains(search)
                                       || s.LastName.Contains(search));
            }
            switch (sort)
            {
                case "firstName":
                    contacts = contacts.OrderBy(s => s.FirstName);
                    break;
                case "lastName":
                    contacts = contacts.OrderBy(s => s.LastName);
                    break;
                case "birthday":
                    contacts = contacts.OrderBy(s => GetDaysUntilBirthday(s.Birthday));
                    break;
                default:
                    contacts = contacts.OrderBy(s => s.FirstName);
                    break;
            }
            var result = await PaginatedList<Contact>.CreateAsync(contacts.AsNoTracking(), page, size);
            Request.HttpContext.Response.Headers.Add("Has-Next", result.HasNextPage ? "true" : "");
            Request.HttpContext.Response.Headers.Add("Count", result.TotalCount.ToString());
            return Ok(result);
        }

        private int GetDaysUntilBirthday(DateTime birthday)
        {
            var nextBirthday = birthday.AddYears(DateTime.Today.Year - birthday.Year);
            if (nextBirthday < DateTime.Today)
            {
                nextBirthday = nextBirthday.AddYears(1);
            }
            return (nextBirthday - DateTime.Today).Days;
        }
        // GET: api/Contacts/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetContact([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var contact = await _context.Contacts.SingleOrDefaultAsync(m => m.Id == id);

            if (contact == null)
            {
                return NotFound();
            }

            return Ok(contact);
        }

        // PUT: api/Contacts/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutContact([FromRoute] int id, [FromBody] Contact contact)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != contact.Id)
            {
                return BadRequest();
            }

            _context.Entry(contact).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ContactExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Contacts
        [HttpPost]
        public async Task<IActionResult> PostContact([FromBody]ContactViewModel contact)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var cont = new Contact
            {
                FirstName = contact.FirstName,
                LastName = contact.LastName,
                Email = contact.Email,
                Address = contact.Address,
                WorkPlace = contact.WorkPlace,
                Notes = contact.Notes,
                Position = contact.Position,
                Birthday = contact.Birthday,
                Phone = contact.Phone,
                PhotoUrl = contact.PhotoUrl
            };
            _context.Contacts.Add(cont);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetContact", new { id = contact.Id }, contact);
        }

        // DELETE: api/Contacts/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteContact([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var contact = await _context.Contacts.SingleOrDefaultAsync(m => m.Id == id);
            if (contact == null)
            {
                return NotFound();
            }

            _context.Contacts.Remove(contact);
            await _context.SaveChangesAsync();

            return Ok(contact);
        }

        private bool ContactExists(int id)
        {
            return _context.Contacts.Any(e => e.Id == id);
        }
    }
}
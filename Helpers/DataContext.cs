using Microsoft.EntityFrameworkCore;
using InTouch.Models;
namespace InTouch.Helpers
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options) { }

        public DbSet<User> Users { get; set; }
        public DbSet<Contact> Contacts { get; set; }

    }
}
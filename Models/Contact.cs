﻿using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;
namespace InTouch.Models
{
    public class Contact
    {
        public int Id { get; set; }

        [Required]
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string WorkPlace { get; set; }
        public string Position { get; set; }
        public string Notes { get; set; }
        public DateTime Birthday { get; set; }
        public string Phone { get; set; }
        public byte[] Avatar { get; set; }
        public string PhotoUrl { get; set; }
    }

    public class ContactViewModel
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string WorkPlace { get; set; }
        public string Position { get; set; }
        public string Notes { get; set; }
        public DateTime Birthday { get; set; }
        public string Phone { get; set; }
        public string PhotoUrl { get; set; }
    }
}

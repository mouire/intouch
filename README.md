# InTouch

###Analog of a note in the phone.

The project was started as a task at the university, but it is not finished.

Therefore, only basic operations and authentication are implemented, but registered users do not have additional capabilities.

### Tech stack
* React, Redux, Atomic Design, Styled-components
* SQL Server
* ASP.NET Core Web API
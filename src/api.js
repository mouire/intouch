﻿var Promise = global.Promise = require('promise');
var superagent = require('superagent-promise')(require('superagent'), Promise);

const responseBody = res => res.body;

const authHeader = req => {
    let token = localStorage.getItem('jwt');
    if (token) {
        req.set('Authorization', ('Bearer ' + token));
    } 
}

const api = {
    del: url =>
        superagent
            .del(url)
            .use(authHeader)
            .then(responseBody),
    get: url =>
        superagent
            .get(url)
            .use(authHeader)
            .then(responseBody),
    put: (url, body) =>
        superagent
            .put(url)
            .send(body)
            .set('Content-type', 'application/json')
            .use(authHeader)
            .then(responseBody),
    post: (url, body) =>
        superagent
            .post(url)
            .send(body)
            .set('Content-type', 'application/json')
            .use(authHeader)
            .then(responseBody),
};

const contacts = {
    all: (paging) =>
        superagent
            .get(`/api/contacts`)
            .query({ page: paging.page+1 })
            .query({ search: paging.search })
            .query({ size: 5 })
            .query({ sort: paging.sort })
            .query({ filter: paging.filter })
            .set('Content-type', 'application/json')
            .use(authHeader),
    get: id =>
        api.get(`/api/contacts/${id}`),
    add: contact =>
        api.post(`/api/contacts`, contact),
    del: id =>
        api.del(`/api/contacts/${id}`),
    edit: contact =>
        api.put(`/api/contacts/${contact.id}`, contact)
};

const auth = {
    register: (user) =>
        api.post(`/users/register`, user),
    login: (username, password) =>
        api.post(`/users/authenticate`, { firstName: username, lastName: username,  username, password })
};

export default {
    contacts,
    auth,
    setToken: _token => { token = _token; }
};


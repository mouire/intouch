import * as React from 'react'
import * as ReactDOM from 'react-dom'
import { BrowserRouter } from 'react-router-dom'
import { injectGlobal } from 'styled-components'

import { globalStyles } from 'ui/theme'
import configureStore from 'configureStore'
import { Root } from 'routes'
import 'css/site.css'


const store = configureStore(window.initialStore || {})
injectGlobal`${globalStyles}`

function renderApp() {
  ReactDOM.render(
    <BrowserRouter>
      <Root store={store} />
    </BrowserRouter>,
    document.getElementById('react-app')
  )
}

renderApp()

if (module.hot) {
  module.hot.accept(
    ['./routes', './ui/theme', './configureStore'],
      renderApp,
  )
}
  
﻿import { createStore, applyMiddleware, combineReducers } from 'redux'
import { createLogger } from 'redux-logger'
import { promiseMiddleware, localStorageMiddleware } from 'middleware'
import auth from 'features/auth/reducer'
import contact from 'features/contact/reducer'
import common from 'features/reducer'


const reducer = combineReducers({
    auth,
    common,
    contact
})

export default function configureStore(initialState) {
  const store = createStore(
    reducer,
    initialState,
    applyMiddleware(
      promiseMiddleware,
      localStorageMiddleware,
      createLogger()
    )
  )
  return store
}
﻿import {
    UPDATE_FIELD_AUTH,
    REGISTER, LOGIN
} from '../../types';

import api from '../../api'

export const login = (username, password) => ({
    type: LOGIN,
    payload: api.auth.login(username, password)
})
export const register = (user) => ({
    type: REGISTER,
    payload: api.auth.register(user)
})
export const onChange = (key, value) => ({
    type: UPDATE_FIELD_AUTH,
    key, value
})

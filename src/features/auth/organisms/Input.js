﻿import React, { Component } from 'react'
import PropTypes from 'prop-types'
import styled, { css } from 'styled-components'


export const Input = styled.input`
    display: block;
	appearance: none;
	outline: 0;
	border: 1px solid rgba(255, 255, 255, 0.4);
	background-color: rgba(255, 255, 255, 0.2);
	width: 250px;
	border-radius: 3px;
	padding: 10px 15px;
	margin: 0 auto 10px auto;
	display: block;
	text-align: center;
	font-size: 18px;
	color: white;
	transition-duration: 0.25s;
	font-weight: 400;
	&:hover{
		background-color: rgba(255, 255, 255, 0.4);
	}
	&:focus{
		background-color: white;
		width: 300px;
		color: #502B5A;
	}

`


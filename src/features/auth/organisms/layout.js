﻿import * as React from 'react';
import styled, { css, keyframes } from 'styled-components'
import { Link } from 'react-router-dom'

export const Layout = styled.div`
display: flex;
align-items: center;
justify-content: center;
width: 100vw;
height:80vh;
flex-direction: column;
& > * {
    margin: 0.5rem;
}

`

const Font = styled.div`
font-size: 1.3rem;
color: white;
padding: 1rem;
font-weight: 300;
display: inline;
`
const To = styled.div`
font-size: 1.3rem;
color: white;
font-weight: 400;
display: inline;
`
export const Title = ({ title, link }) => <Font>
    {title}
    <Link to={link} >
        <To> Go here.</To>
    </Link>
</Font>
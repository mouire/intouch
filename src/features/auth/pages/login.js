﻿import React from 'react';
import { connect } from 'react-redux';
import { Button } from 'antd';
import { login, onChange } from '../actions'
import { Layout, Title, Input } from '../organisms'

const mapStateToProps = state => ({ ...state.auth });

const mapDispatchToProps = dispatch => ({
    onChange: (key, value) =>
        dispatch(onChange(key, value)),
    onLogin: (email, password) =>
        dispatch(login(email, password))
});

class LoginForm extends React.Component {

    changeEmail = ev => this.props.onChange('email', ev.target.value);

    changePassword = ev => this.props.onChange('password', ev.target.value);

    onLogin = (email, password) => ev => {
        ev.preventDefault();
        this.props.onLogin(email, password);
    }
   
    render() {
        const { email, password } = this.props;
        return (
            <Layout>
                <Input name='email'
                    placeholder="Enter your username"
                    value={email}
                    onChange={this.changeEmail}
                />
                <Input name='password'
                    placeholder="Enter your password"
                    value={password}
                    onChange={this.changePassword}
                />
                <Button ghost size='large'
                    onClick={this.onLogin(email, password)} >
                   Login
               </Button>

                <Title
                    link='/register'
                    title="Don't have an account?"
                />
            </Layout>
        );
    }
}

export const Login = connect(mapStateToProps, mapDispatchToProps)(LoginForm);


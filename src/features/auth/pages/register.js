﻿import React from 'react'
import { connect } from 'react-redux'
import { Layout,Title, Input } from '../organisms'
import { Button } from 'antd';
import { onChange, register} from '../actions'
const mapStateToProps = state => ({ ...state.auth })

const mapDispatchToProps = dispatch => ({
    onChange: (key, value) =>
        dispatch(onChange(key, value)),
    onRegister: user =>
        dispatch(register(user))
});
class RegisterForm extends React.Component {

    onChange = e => this.props.onChange(e.target.name, e.target.value);
   

    onRegister = (name, username, password) => ev => {
        ev.preventDefault();
        this.props.onRegister({ name, username , password });
    }
    render() {
        const { name, username, password } = this.props;
        return (
            <Layout>
                <Input name='name'
                    placeholder="Name"
                    value={name}
                    onChange={this.onChange}
                />
                <Input name='username'
                    placeholder="Username"
                    value={username}
                    onChange={this.onChange}
                />
                <Input name='password'
                    placeholder="Password"
                    value={password}
                    onChange={this.onChange}
                />
                <Button ghost size='large'
                    onClick={this.onRegister(name, username, password)} >
                    Login
               </Button>
                <Title
                    link='/login'
                    title="Already have an account?"
                />
            </Layout>
        );
    }
}

export const Register = connect(mapStateToProps, mapDispatchToProps)(RegisterForm);


﻿import {
    LOGIN,
    REGISTER,
    ASYNC_START,
    ASYNC_END,
    UPDATE_FIELD_AUTH
} from '../../types';

export default (state = {}, action) => {
    switch (action.type) {
        case ASYNC_START:
            if (action.subtype === LOGIN || action.subtype === REGISTER) {
                return { ...state, inProgress: true };
            }
            break;
        case ASYNC_END:
            if (action.subtype === LOGIN || action.subtype === REGISTER) {
                return { ...state, inProgress: false, payload: action.payload };
            }
            break;
        case UPDATE_FIELD_AUTH:
            return { ...state, [action.key]: action.value };
        case REGISTER: return {};
        case LOGIN: return {};
        default:
            return state;
    }

    return state;
};
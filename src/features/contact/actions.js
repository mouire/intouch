﻿import {
    CHANGE_FIELD,
    GET_CONTACT_FROM_API,
    DELETE_CONTACT,
    EDIT_CONTACT,
    ADD_CONTACT,
    SEARCH_CONTACT,
    BLUR_FIELD,
    ADD_CONTACT_FORM_LOAD,
    EDIT_CONTACT_FORM_LOAD,
    LOAD_CONTACTS,
    GET_CONTACT_FROM_STORE,
    HOME_PAGE_UNLOADED
} from 'types'
import api from 'api'
import { validate } from 'features/validations'

export const search = () => ({
    type: SEARCH_CONTACT
})

export const all = (paging) => ({
    type: LOAD_CONTACTS,
    payload: api.contacts.all(paging),
    paging: paging
})

export const update = (key, value) => ({
    type: CHANGE_FIELD,
    key, value
})

export const blur = (key, value) => ({
    type: BLUR_FIELD,
    error: validate(key, value),
    key, value
})
export const apiById = (id) => ({
    type: GET_CONTACT_FROM_API,
    payload: api.contacts.get(id)
})

export const storeById = (id) => ({
    type: GET_CONTACT_FROM_STORE,
    id
})

export const deleteContact = (id) => ({
    type: DELETE_CONTACT,
    payload: api.contacts.del(id)
})
export const edit = (contact) => ({
    type: EDIT_CONTACT,
    payload: api.contacts.edit(contact)
})
export const add = (contact) => ({
    type: ADD_CONTACT,
    payload: api.contacts.add(contact)
})
export const unload = () => ({
    type:   HOME_PAGE_UNLOADED
})

export const createForm = (type) => ({
    type: type === 'new' ? ADD_CONTACT_FORM_LOAD : EDIT_CONTACT_FORM_LOAD
})

﻿import React from 'react'
import styled from 'styled-components'
import { Icon , Spin} from 'antd'
import ReactCrop, { makeAspectCrop } from 'react-image-crop'
import Dropzone from 'react-dropzone'
import request from 'superagent'
import 'react-image-crop/dist/ReactCrop.css'


const CLOUDINARY_UPLOAD_PRESET = 'wgncshp5'
const CLOUDINARY_UPLOAD_URL = 'https://api.cloudinary.com/v1_1/mouire/upload'

export class Avatar extends React.Component {
    state = {
        selectedImageURL: this.props.src,
        selectedFile: null,
        croppedImage: this.props.src,
        disableDrop: false
    };

    onCropChange = crop => this.setState({ crop })

    handleCropClose = () => {
        let { crop, selectedImageURL } = this.state;
        const croppedImg = getCroppedImg(this.refImageCrop, crop, selectedImageURL);
        this.setState({
            disableDrop: false,
            croppedImage: croppedImg.path
        })
        this.onSave(croppedImg.file)
    }

    onSave = file => request.post(CLOUDINARY_UPLOAD_URL)
        .field('upload_preset', CLOUDINARY_UPLOAD_PRESET)
        .field('file', file)
        .then(response => {
            response.body.secure_url !== '' && this.setState({
                imageUrl: response.body.secure_url
            });
            this.props.onChange(response.body.secure_url)
        })

    onImageLoaded = image => this.setState({
        crop: makeAspectCrop({
            x: 0,
            y: 0,
            aspect: 1 / 1,
            width: 100,
        }, image.width / image.height)
    })

    onImageDrop = files => this.setState({
        selectedImageURL: files[0].preview,
        disableDrop: true
    })

    render() {
        const { onImageDrop, onImageLoaded, handleCropClose, onCropChange} = this,
            { disableDrop, crop, selectedImageURL, croppedImage } = this.state;
        return (
            <Wrapper>
                <Dropzone
                    onDrop={onImageDrop}
                    multiple={false}
                    accept="image/*"
                    disableClick={disableDrop}
                    className={disableDrop ? 'dropzone' : 'default'}
                >
                    {
                        disableDrop ? <ReactCrop
                                src={selectedImageURL}
                                crop={crop}
                                onChange={onCropChange}
                                onImageLoaded={onImageLoaded} /> : <Add>
                                <Icon type="plus-circle" style={icon} />
                                <img src={croppedImage} style={{ width: '18vh' }} />
                            </Add>
                    }
                </Dropzone>

                <img src={selectedImageURL} style={{ display: 'none' }} ref={(img) => { this.refImageCrop = img }} />
                {
                    disableDrop &&
                    <Icon onClick={handleCropClose}
                        shape="circle"
                        type="check"
                        style={{ margin: '0.5rem', fontSize: '2rem', color: 'white' }}
                    />
                }
            </Wrapper>
        )
    }
}

const Spinner = () => <Spin
    indicator={<Icon type="loading" spin style={{ fontSize: '2rem' }} />}
    style={{ margin: '1rem' }}
/>

const Wrapper = styled.div`
    display:flex;
    flex-direction:column;
    align-items:center;
    margin: 1rem;
`
const Add = styled.div`
    width: 100%;
    height: 100%;
    display: flex;
    align-items: center;
    justify-content: center;
`

const icon = {
    fontSize: '3rem',
    color: 'white',
    position: 'absolute',
    left: 0,
    right: 0,
    margin: 'auto'
}

const getCroppedImg = (srcImage, pixelCrop, src) => {
                    let img = new Image();
    img.src = src;
    const targetX = srcImage.width * pixelCrop.x / 100;
    const targetY = srcImage.height * pixelCrop.y / 100;
    const targetWidth = srcImage.width * pixelCrop.width / 100;
    const targetHeight = srcImage.height * pixelCrop.height / 100;

    const canvas = document.createElement('canvas');
    canvas.width = targetWidth;
    canvas.height = targetHeight;
    const ctx = canvas.getContext('2d');

    ctx.drawImage(
        img,
        targetX,
        targetY,
        targetWidth,
        targetHeight,
        0,
        0,
        targetWidth,
        targetHeight
    );

    const path = canvas.toDataURL('image/png');
    var blobBin = atob(path.split(',')[1]);
    var array = [];
    for (var i = 0; i < blobBin.length; i++) {
                    array.push(blobBin.charCodeAt(i));
                }
    var file = new Blob([new Uint8Array(array)], {type: 'image/png' });
    return ({ path, file });
}



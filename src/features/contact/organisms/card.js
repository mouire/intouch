﻿import * as React from 'react';
import { Link } from 'react-router-dom'
import moment from 'moment'
import { Card, Icon, Avatar, Popconfirm } from 'antd';
const { Meta } = Card;
import styled from 'styled-components'



const Main = styled.div`
display: flex;
flex-direction:column;
align-items: flex-start;
padding: 0 2rem;
`

const Options = styled.div`
    position: absolute;
    right: 0;
    top: 0;
    display: flex;
    align-items: center;
    height: 100%;
    padding: 1rem;
    & > * {
        margin: 0.5rem;
    }
`

const Wrapper = styled.div`
    width: 75%;
    @media (orientation: portrait) {
        width: 100%;
    }
    display: flex;
    justify-content: space-between;
    position: relative;
    margin: 1rem auto;
`

export default class ContactCard extends React.Component {
    
    render() {
        const { contact, onDelete, onEdit } = this.props;
        const left = moment(contact.birthday).dayOfYear()-moment().dayOfYear();
        return <Wrapper>
                <Card style={{width: '100%', background: 'rgba(255,255,255,0.5)', borderRadius: '1rem'}}>
                <Link to={"/contact/" + contact.id}>
                    <Meta
                        title={contact.firstName+' '+ contact.lastName}
                        avatar={(
                            <Avatar 
                                size='large' 
                                style={{ width: '80px' , height: '80px'}} 
                                src={contact.photoUrl || 'https://png.icons8.com/flat_round/1600/sheep.png' } 
                            />
                        )}
                        description={(
                            <Main>
                                <div>{contact.phone} </div>
                                <div> 
                                    {moment(contact.birthday).format('L')}
                                </div>
                                <div> 
                                    {(left <100 && left > 0)? ` ${left} days left` : ''} 
                                </div>
                            </Main>
                        )}
                    />
                </Link>
                <Options>
                    <Link to={"/contact/" + contact.id+'/edit'}>
                        <Icon type="edit" onClick={onEdit} style={{ fontSize: '1.5rem'}} />
                    </Link>
                    <Popconfirm
                        placement="right"
                        title="Are you sure?"
                        onConfirm={onDelete}
                        okText="Yes"
                        okType="danger"
                        cancelText="No"
                    >
                        <Icon type="delete" style={{ fontSize: '1.5rem'  }} />
                    </Popconfirm>
                </Options>
                </Card>
                </Wrapper>;
    }
}


﻿import * as React from 'react'
import styled from 'styled-components'
import { connect } from 'react-redux'
import { Input, Select } from 'antd'
import { search, all } from '../actions'


const Search = Input.Search,
    Option = Select.Option;

const mapStateToProps = state => ({
    search: state.contact.search,
    sort: state.contact.sort,
    filter: state.contact.filter
});

const mapDispatchToProps = dispatch => ({
    onChange: () => dispatch(search()),
    refresh: paging => dispatch(all(paging))
});

class ContactsList extends React.Component {

    onSort = value => {
        this.props.onChange()
        const {filter, search, sort } = this.props;
        this.props.refresh({ page: 0, filter, search, sort: value })
    }

    onFilter = e => this.props.onChange('filter', e.target.value)

    onSearch = value => {
        this.props.onChange()
        const { filter, search, sort } = this.props;
        this.props.refresh({ page: 0, filter, search: value, sort })
    } 
    
    render() {
        const { sort, search, filter } = this.props,
            { onSearch, onFilter, onSort } = this;
        return (
            <Wrapper>
                <Select placeholder="Sort" style={{width: '20vw', margin: '0 1rem'}} onChange={onSort}>
                    <Option value="firstName">First Name</Option>
                    <Option value="lastName">Last Name</Option>
                    <Option value="birthday">Birthday</Option>
                </Select>
                <Search
                    placeholder="Find"
                    onSearch={onSearch}
                    enterButton
                />
            </Wrapper>
        );
    };

}


export default connect(mapStateToProps, mapDispatchToProps)(ContactsList);

const Wrapper = styled.div`
display: flex;
    & > * {
    margin: 1rem;
    }
`
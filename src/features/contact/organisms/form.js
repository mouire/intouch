﻿import React, { Component } from 'react'
import { Avatar } from './avatar'
import { Form, Field, Calendar} from 'ui'

export default class ContactForm extends Component {
    onChange = e => {
        let name = e.target.name,
            value = e.target.value.trim();
        this.props.onChange(name, value);
       (this.props.touched[name] && this.props.errors[name].length > 0) && this.onBlur(e);
    }

    onChangeWithoutBlur = e => this.props.onChange(e.target.name, e.target.value)

    onBlur = e => (e.target.value.length > 0) && this.props.onBlur(e.target.name, e.target.value)

    onAgeChange = (date, dateString) => this.props.onChange('birthday', dateString)

    onImage = photo => this.props.onChange('photoUrl', photo)

    render() {
        const { onChangeWithoutBlur, onImage, onChange, onBlur} = this;
        const { contact, onSubmit, errors, header, touched } = this.props;
        return <Form onSubmit={onSubmit} header={header} >
            <Avatar
                src={contact.photo}
                onChange={onImage}
            />
            <Field name='firstName'
                value={contact.firstName}
                onChange={onChange}
                onBlur={onBlur}
                touched={touched.firstName}
                error={errors.firstName}
                label="First Name"
                required
            />
            <Field value={contact.lastName}
                name='lastName'
                onChange={onChange}
                label="Last Name"
            />
            <Calendar
                onChange={this.onAgeChange}
                label='Birthday'
            />
            <Field 
                required
                name='phone'
                value={contact.phone}
                onChange={onChange}
                onBlur={onBlur}
                label="Phone"
                touched={touched.phone}
                error={errors.phone}
                addonBefore="+380"
                />
            <Field name='email'
                value={contact.email}
                onChange={onChange}
                onBlur={onBlur}
                error={errors.email}
                touched={touched.email}
                label='Email'
            />
            <Field name='address'
                value={contact.address}
                onChange={onChangeWithoutBlur}
                label='Address'
            />
        </Form>
    }
}




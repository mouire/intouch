﻿import React, { Component } from 'react'
import { connect } from 'react-redux'
import moment from 'moment'
import { ContactForm } from '../organisms'
import { createForm, add, update, blur} from '../actions'


const randomBirthday = {
    day: Math.floor(Math.random() * (30 - 1)) + 1,
    month: Math.floor(Math.random() * (13 - 1)) + 1,
    year: Math.floor(Math.random() * (2005 - 1970)) + 1970,
    
}

const mapStateToProps = state => ({
    contact: state.contact.edit,
    errors: state.contact.errors,
    touched: state.contact.touched
});

const mapDispatchToProps = dispatch => ({
    onChange: (name, value) => dispatch(update(name, value)),
    onBlur: (name, error) => dispatch(blur(name, error)),
    onLoad: () => dispatch(createForm('new')),
    onSubmit: contact => dispatch(add(contact))
});

class NewContact extends Component {
    componentDidMount = () => this.props.onLoad()

    onSubmit = e => {
        e.preventDefault();
        const { contact, touched, errors } = this.props,
            { firstName, lastName, email, photoUrl, address, workPlace, position, notes, birthday, phone } = this.props.contact;
        const submit = (touched.firstName && touched.phone) && Object.values(errors).every(err => err.length === 0);
        submit && this.props.onSubmit({ 
            firstName, 
            lastName: lastName || '', 
            email, 
            photoUrl, 
            address, 
            birthday: birthday || moment(randomBirthday), 
            phone 
        });
        this.props.history.replace('/');
    }
    render() {
        const { contact, touched, errors } = this.props;
        return <ContactForm
            onBlur={this.props.onBlur}
            onChange={this.props.onChange}
            onTagsChange={this.props.onTagsChange}
            onSubmit={this.onSubmit}
            header='New contact'
            contact={contact}
            touched={touched}
            errors={errors}
        />
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(NewContact);

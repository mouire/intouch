﻿import React from 'react'
import { connect } from 'react-redux'
import moment from 'moment'
import { Cloud } from 'ui'
import { Spin, Avatar, Steps, Icon} from 'antd'
import { storeById, apiById } from '../actions'


const Step = Steps.Step

const mapStateToProps = (state, ownProps) => ({
    contact: state.contact.contactById,
    historyId: ownProps.match.params.id,
    byId: state.contact.byId
});

const mapDispatchToProps = dispatch => ({
    fromStore: id => dispatch(storeById(id)),
    fromApi: id => dispatch(apiById(id))
});
class Contact extends React.Component {
    
    componentWillMount = () => {
        (this.props.byId && this.props.byId.indexOf(parseInt(this.props.historyId)) !== -1) ? 
            this.props.fromStore(this.props.historyId) :
            this.props.fromApi(this.props.historyId)
    }


    render() {
        const { contact } = this.props;
        if (!this.props.contact) return <Spin />
        return <Cloud>
            <Avatar style={{ width: '20vh', height: '20vh', marginTop: '5vh' }}
                src={contact.photoUrl || "https://png.icons8.com/flat_round/1600/sheep.png"} />
            
            <h4 style={{ padding: '2rem', fontSize: '2rem', color: 'white' }}>
                {contact.firstName} {contact.lastName}
            </h4>

            <Steps direction="vertical" size="small" style={icon} >
                <Step 
                    icon={<Icon type="mobile" style={icon} />}
                    title="Phone"
                    description={contact.phone} />
                <Step 
                    icon={<Icon type="message" style={icon} />}
                    title="Email"
                    description={contact.email} />
                <Step 
                    icon={<Icon type="home" style={icon} />}
                    title="Address"
                    description={contact.address} />
                <Step 
                    title="Birthday"
                    description={(
                        <div> 
                            {moment(contact.birthday).format('L')}
                            <div> 
                                {
                                    (moment(contact.birthday).dayOfYear()-moment().dayOfYear() < 100 
                             && moment(contact.birthday).dayOfYear()-moment().dayOfYear() > 0   )?
                                     `${moment(contact.birthday).dayOfYear()-moment().dayOfYear() } days left` :
                                      ''} 
                            </div>
                        </div>
                    )}
                    icon={<Icon type="gift" style={icon} />}
                />
            </Steps>
        </Cloud>
    }
}



const icon = {
    color: 'white'
}

export default connect(mapStateToProps, mapDispatchToProps)(Contact);

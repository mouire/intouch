﻿import React, { Component } from 'react'
import { connect } from 'react-redux';

import { ContactForm } from '../organisms'
import { createForm, edit, update, blur } from '../actions';

const mapStateToProps = state => ({
    contact: state.contact.edit,
    errors: state.contact.errors,
    touched: state.contact.touched
});

const mapDispatchToProps = dispatch => ({
    onChange: (name, value) => dispatch(update(name, value)),
    onBlur: (name, error) => dispatch(blur(name, error)),
    onLoad: () => dispatch(createForm('edit')),
    onSubmit: contact => dispatch(edit(contact))
});

class NewContact extends Component {
    componentWillMount = () => this.props.onLoad()

    onSubmit = e => {
        e.preventDefault();
        const { contact, touched, errors } = this.props,
            { id, firstName, lastName, email, photoUrl, address, workPlace, position, notes, birthday, phone } = this.props.contact;
        Object.values(errors).every(err => err.length === 0) && this.props.onSubmit({
            id,
            firstName,
            lastName, email,
            photoUrl, address,
            workPlace, position,
            notes, birthday, phone
        });
        this.props.history.replace(`/contact/${id}`);
    }
    render() {
        const { contact, touched, errors } = this.props;
        return <ContactForm
            onBlur={this.props.onBlur}
            onChange={this.props.onChange}
            onTagsChange={this.props.onTagsChange}
            onSubmit={this.onSubmit}
            header='Edit contact'
            contact={contact}
            touched={touched}
            errors={errors}
        />
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(NewContact);

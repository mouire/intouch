﻿import * as React from 'react'
import { ContactCard } from '../organisms'
import styled from 'styled-components'
import { connect } from 'react-redux'
import InfiniteScroll from 'react-infinite-scroller'
import { all, deleteContact, storeById, unload} from '../actions'
import { Spin, List, Icon} from 'antd'

const mapStateToProps = state => ({
    ...state.contact
});

const mapDispatchToProps = dispatch => ({
    onLoad: paging => dispatch(all(paging)),
    onDelete: id => dispatch(deleteContact(id)),
    getContact: id => dispatch(storeById(id)),
    onUnload: () => dispatch(unload()),
});

class ContactsList extends React.Component {

    componentWillMount = () => {
        const { count, totalCount } = this.props;
        count === 0 && this.onLoad()
    }
    componentWillUnmount = () => this.props.onUnload()

    onLoad = () => {
        const { page, filter, search, sort } = this.props;
        this.props.onLoad({ page, filter, search, sort })
    }

    onDelete = id => {
        this.props.onDelete(id)
        this.onLoad()
    }

    onEdit = id => this.props.getContact(id)

    render() {
        const { contacts, hasNext } = this.props;
        const spinner = <Spin indicator={<Icon type="loading" spin style={{ fontSize: '2rem' }} />} style={{margin: '1rem'}}/>;

        if (!contacts) return spinner

        if (this.props.contacts.length === 0)
            return <div>There are no contacts</div>

        return <InfiniteScroll pageStart={1}
            loadMore={this.onLoad}
            hasMore={hasNext}
            loader={spinner}>
            <Wrapper>
            <List style={{ width: '100%', margin: '2rem auto' }}>
                {
                    this.props.contacts.map((contact, i) =>
                        <ContactCard key={i}
                            contact={contact}
                            onDelete={() => this.onDelete(contact.id)}
                            onEdit={() => this.onEdit(contact.id)}
                        />)
                }
                </List>
                </Wrapper>
        </InfiniteScroll >
    };

}


export default connect(mapStateToProps, mapDispatchToProps)(ContactsList);

const Wrapper = styled.div`

    @media (orientation: portrait) {
        width: 80vw;
    }
    @media (orientation: landscape) {
        width: 60vw;
    }
`
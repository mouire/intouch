﻿import {
    HOME_PAGE_LOADED, HOME_PAGE_UNLOADED,
    ASYNC_START,
    ASYNC_END,
    GET_CONTACT_FROM_API,
    GET_CONTACT_FROM_STORE,
    DELETE_CONTACT,
    EDIT_CONTACT,
    ADD_CONTACT,
    CHANGE_FIELD,
    BLUR_FIELD, SEARCH_CONTACT,
    CHANGE_SEARCH_CONTACT,
    ADD_CONTACT_FORM_LOAD,
    EDIT_CONTACT_FORM_LOAD,
    LOAD_CONTACTS, APP_LOAD
} from 'types'


const initial = {
    edit: {},
    errors: {},
    touched: {},
    filter: 'name',
    search: null,
    sort: 'firstName',
    page: 0,
    count: 0,
    totalCount: 0
}

export default (state = initial, action) => {
    switch (action.type) {
        case SEARCH_CONTACT:
            delete state.contacts;
            delete state.byId;
            return {
                ...state
            };
        case LOAD_CONTACTS:
            const { filter, sort, search, page} = action.paging;
            return {
                ...state,
                filter, sort, search,
                contacts: (state.contacts || []).concat(action.payload.body),
                byId: (state.byId || []).concat(action.payload.body.map(e => e.id)),
                hasNext: Boolean(action.payload.headers['has-next']),
                totalCount: parseInt(action.payload.headers['count']),
                count: (state.contacts ? state.contacts.length : 0 ) + action.payload.body.length,
                page: page+1
            };
        case DELETE_CONTACT:
        case EDIT_CONTACT:
            return {
                ...state,
                contactById: action.payload
            };
        case ADD_CONTACT:  
            return {
                ...state,
                edit: {},
                errors: {},
                touched: {}
            };
        case CHANGE_FIELD:
            return {
                ...state,
                edit: {
                    ...state.edit,
                    [action.key]: action.value
                }
            };
        case BLUR_FIELD:
            return {
                ...state,
                errors: {
                    ...state.errors,
                    [action.key]: action.error
                },
                touched: {
                    ...state.touched,
                    [action.key]: true
                }
            };
        case GET_CONTACT_FROM_API:
            return {
                ...state,
                contactById: action.payload
            };
        case GET_CONTACT_FROM_STORE:
            const id = state.byId.indexOf(parseInt(action.id))
            return {
                ...state,
                contactById: state.contacts[id],
                idGet: id
            };
        case ADD_CONTACT_FORM_LOAD:
            return {
                ...state,
                edit: {},
                errors: {},
                touched: {}
            };
        case EDIT_CONTACT_FORM_LOAD:
            return {
                ...state,
                edit: {
                    ...state.contactById
                }
            };
        default:
            return state;
    }
}

﻿import * as React from 'react';
import { ContactsList, Filter } from 'features'
import styled from 'styled-components'


export const Home = () => <Wrapper>
    <h2>Contacts</h2>
    <Filter />
    <ContactsList />
</Wrapper>

const Wrapper = styled.div`
    display:flex;
    align-items: center;
    padding: 3rem 2rem;
    flex-direction: column;
    text-align:center;
    width: 90vw;
    margin: 0 auto;
`

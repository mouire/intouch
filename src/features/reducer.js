﻿import {
    APP_LOAD,
    DELETE_CONTACT,
    EDIT_CONTACT,
    ADD_CONTACT,
    REDIRECT,
    ASYNC_START,
    ASYNC_END,
  LOGIN,
  REGISTER, LOGOUT
} from 'types';

const defaultState = {
    appName: 'Note',
    token: null,
    loading: false
};

export default (state = defaultState, action) => {
    switch (action.type) {
        case APP_LOAD:
            return {
                ...state,
                token: action.token || null,
                appLoaded: true,
                currentUser: action.payload ? action.payload.user : null
            };
        case ASYNC_START:
            return { ...state, loading: true };
        case ASYNC_END:
            return { ...state, loading: false };
        case LOGIN:
            return {
            ...state,
            token: action.payload.token
            };
        case REGISTER:
            return {
                ...state,
                redirectTo: action.error ? null : '/',
                token: action.payload.token,
                currentUser: action.error ? null : action.payload.user
            };
        case REDIRECT:
            return { ...state, redirectTo: null };
        case DELETE_CONTACT:
            return { ...state, redirectTo: '/' };
        case ADD_CONTACT:
            return { ...state };
        case EDIT_CONTACT:
            return { ...state, redirectTo: '/' };
        default:
            return state;
    }
};
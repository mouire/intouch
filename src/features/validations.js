﻿export const validate = (name, value) => {
    let error = String(value).replace(/\s+/g, "").length === 0 ?
        'Required' : rules[name](value);
    return error;
};

var rules = {
    firstName: value => {
        let error = (value.length <= 30 && value.length >= 3 && value.search(/\d/) === -1) ? '' : "Введіть ім'я";
        return error;
    },
    email: value => {
        let re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        let regexp = /^([a-z0-9_-]{1,15}\.){0,3}[a-z0-9_-]{1,15}@[a-z0-9_-]{1,15}\.[a-z]{2,6}$/;
        //Пропускаем до 15 символов a-z0-9_- перед собачкой, 
        //также это может быть до 4 слов, разделенных точками.
        //Затем собачка и имя домена (от 1 до 15 символов).
        //Затем доменная зона - от 2 до 6 латинских букв
        let error = regexp.test(value) ? '' : 'Please enter a valid email';
        return error;
    },
    phone: value => {
        //В правиле указываем что первый и последний знак должен быть обязательно цифрой — «\d»,
        //а в середине разрешаем использовать знаки
        //скобок, пробел и знак дефиса — «[\d\(\) \ -]{ 4, 14 } », от 4 до 14 символов.
        //Так как скобки и пробел являются зарезервированными элементами регулярных выражений,
        //перед ними ставим обратный слеш.
        let re = /^\d[\d\(\)\ -]{4,14}\d$/;
        let len = value.replace(/[^-0-9]/gim, '').length;
        let error = !re.test(value) ? 'It seems that in the number not only the figures'
            : (len !== 10) ? 'The phone must contain 10 digits' : '';
        return error;
    }
}
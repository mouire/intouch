import * as React from 'react';
import { Route } from 'react-router-dom'
import { Provider } from 'react-redux'

import { App } from 'ui'
import {
    ContactPage,
    NewContact,
    EditContact
} from 'features'
import { Login, Register } from 'features/auth/pages'
import { Home } from 'features/pages'


export const Root = ({ store }) => (
    <Provider store={store}>
        <App>
    <Route exact path='/' component={Home } />
    <Route path='/new' component={NewContact} />
    <Route path='/login' component={Login} />
    <Route path='/register' component={Register} />
            <Route exact path='/contact/:id' component={ContactPage} />
            <Route exact path='/contact/:id/edit' component={EditContact} />
        </App>
    </Provider>
)

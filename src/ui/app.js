﻿import * as React from 'react'
import styled, { keyframes } from 'styled-components'
import { Layout, Icon } from 'antd'
import { Menu } from './'
import { Telegram } from 'ui/icons'

const { Header } = Layout

export const App = ({ children }) => <Layout>
    <Header style={{ position: 'fixed', width: '100%', zIndex: '100', background: '#ffffff' }}>
        <Menu />
    </Header>
    <Wrapper>
        <Bubbles />
        <Content>
            {children}
        </Content>
        <Footer style={{display: 'flex', justifyContent: 'center', zIndex: '100'}}>
			<a href='mailto:ashmooree1@gmail.com' target="_blank"><Icon type="google-plus" style={icon} /></a>
			<a href='http://t.me/vmouire' target="_blank"> <img src={Telegram} style={icon} /></a>
        </Footer>
    </Wrapper>
</Layout>

const Bubbles = () => <Bubs>
    <Bubble /><Bubble /><Bubble /><Bubble />
    <Bubble /><Bubble /><Bubble /><Bubble />
    <Bubble /><Bubble /><Bubble /><Bubble />
    <Bubble /><Bubble /><Bubble /><Bubble />
</Bubs>

const Content = styled.div`
    z-index: 3;
    position:relative;
`

const icon = {
	color: 'white',
	fontSize: '3rem',
	height: '3rem',
	width: '3rem',
	margin: '1rem'
}

const Footer = styled.div`
    padding: 2.5rem;
    font-weight: 400;
    color: white;
    text-align: center;
    position: relative;
    bottom: 0;
    width:100%;
    letter-spacing: 0.1rem;
    font-size: 1.1rem;
`

const Bubs = styled.div`
    position: fixed;
	left: 0;
	width: 100vw;
    height: 120vh;
    top: -20vh;
	z-index: 1;
    overflow: hidden;
`

const square = keyframes`
    0% { transform: translateY(0); }
    100% { transform: translateY(-150vh) rotate(600deg); }
`
const Wrapper = styled.div`
    top: 8vh;
    left: 0;
    position: absolute;
    width:100%;
    min-height: 92vh;
    
`

const Bubble = styled.div`
    position: absolute;
    display: block;
    width: 40px;
    height: 40px;
    background-color: rgba(255,255,255, 0.15);
    bottom: -15vh;
    animation: ${square} 25s infinite;

	&:nth-child(1){
		left: 10%;
	}

    &:nth-child(2){
		left: 20%;
		width: 80px;
		height: 80px;
		animation-delay: 2s;
		animation-duration: 17s;
	}
		
	&:nth-child(3){
		left: 25%;
		animation-delay: 4s;
	}
		
	&:nth-child(4){
		left: 40%;
		width: 60px;
		height: 60px;
		animation-duration: 22s;
		background-color: rgba(255,255,255, 0.25);
	}
		
	&:nth-child(5){
		left: 70%;
	}
		
	&:nth-child(6){
		left: 80%;
		width: 120px;
		bottom: -30%;
		height: 120px;
		animation-delay: 3s;
		background-color: rgba(255,255,255,0.2);
	}
		
	&:nth-child(7){
		left: 32%;
		width: 160px;
		height: 160px;
		bottom: -30%;
		animation-delay: 7s;
	}
		
	&:nth-child(8){
		left: 55%;
		width: 20px;
		height: 20px;
		animation-delay: 15s;
		animation-duration: 40s;
	}
		
	&:nth-child(9){
		left: 25%;
		width: 10px;
		height: 10px;
		animation-delay: 2s;
		animation-duration: 40s;
		background-color: rgba(255,255,255, 0.3);
	}
		
	&:nth-child(10){
		left: 90%;
		width: 160px;
		height: 160px;
		bottom: -30%;
		animation-delay: 11s;
	}
`
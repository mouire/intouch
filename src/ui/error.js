﻿import React from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'

import { color } from './theme'


const ErrorText = styled.span`
  display: inline-block;
    box-sizing: border-box;
 color: ${color.danger};
`

export const Error = ({ error, active }) => (
    <ErrorText> {error} </ErrorText>
)


Error.propTypes = {
    error: PropTypes.string,
    active: PropTypes.bool
}

Error.defaultProps = {
    error: '',
    active: false
}
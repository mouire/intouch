﻿import React from 'react'
import styled, { css } from 'styled-components'
import PropTypes from 'prop-types'

import { CloudWrapper } from './cloud'
import { Icon, Button } from 'antd'


const Title = styled.div`
    position:relative;
    font-size: 1.5rem;
    padding: 2rem 0 1rem 0;
    text-transform: uppercase;
    letter-spacing: 0.3rem;
    text-align: center;
    color: white;
`
const Cloud = CloudWrapper.extend`
background: transparent;
    box-shadow: none;
padding: 0;
`

const Back = styled.div`
    position:absolute;
    top:2rem;
    left:2rem;
    z-index: 2;
`
export class Form extends React.Component {
    back = () => this.props.history.goBack();
    render() {
        const { children, header, onSubmit } = this.props;
        return <Cloud>
            <Back>
              <Icon type='arrow-left' style={{ color: '#ffffff', fontSize: '2rem' }} onClick={this.back} />
            </Back>
            <Title>
                {header}
            </Title>
            {children}
            <Button
                onClick={onSubmit} 
                size='large'
            >
            Save
            </Button>
        </Cloud>;
    }
}


Form.propTypes = {
    children: PropTypes.node.isRequired,
    header: PropTypes.string,
}

Form.defaultProps = {
    header: 'Редагувати',
}
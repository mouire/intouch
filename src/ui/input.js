﻿import React from 'react'
import styled, { css } from 'styled-components'
import { Error } from './'
import { DatePicker } from 'antd'
import moment from 'moment'


const fieldHeight = '5rem'
const inputHeight = '4rem'


export const Input = styled.input`
	width: 100%;
	border-radius: 3px;
	display: block;
	color: white;
	transition-duration: 0.25s;
	font-weight: 300;
    background: transparent;
    border: none;
    left: 1rem;
    position:relative;
    height: 100%;
    &:focus{
        border: none;
        outline: none;
    }
`
const FieldContainer = styled.div`
    display: flex;
    flex-direction: column;
    position: relative;
    width: 100%;
    height: ${fieldHeight};
    -webkit-appearance: none;
    flex-shrink: 0;
    align-items: center;
    ${p => p.active && css`
        ${FieldLabel} {
            transform: scale(0.9) translateY(-1rem);
        }
        ${Input} {
            padding-top: 1.8rem;
            padding-bottom: 0.2rem;
        }
    `}
`

const FieldLabel = styled.label`
  margin: 0;
  padding: 0;
  border: 0;
  vertical-align: baseline;
  white-space: nowrap;
  user-select: none;
  transition: transform ease-out .1s;
  transform-origin: left;
  text-overflow: ellipsis;
  right: 0;
  position: absolute;
  pointer-events: none;
  overflow: hidden;
  line-height: ${inputHeight};
  left: 1rem;
  height: ${inputHeight};
font-weight: 400;
`
const FieldWrapper = styled.div`
    display: block;
    appearance: none;
	outline: 0;
	border: 1px solid rgba(255, 255, 255, 0.4);
	background-color: rgba(255, 255, 255, 0.2);
	width: 250px;
	border-radius: 3px;
	display: block;
	color: white;
	transition-duration: 0.25s;
	font-weight: 300;
    height: ${inputHeight};
    position: relative;
    width: 80%;
    display: flex;
    flex-direction: column;
    position: relative;
    top: 0;
    &:hover{
	    background-color: rgba(255, 255, 255, 0.4);
    }
    ${p => p.focus && css`
        &:hover{
	        background-color: white;
        }
        background-color:  white;
        color: #502B5A;
        width: 90%;
        ${FieldLabel, Input} {
            color: #502B5A;
        }
    `}
`

const Req = styled.div`
display: inline;
color: red;
padding: 0 0.5rem;
`

export class Field extends React.Component {

    state = { focus: false }

    onFocus = () => this.setState({ focus: true })

    onBlur = e => {
        this.setState({ focus: false })
        this.props.onBlur(e)
    }

    render() {
        const { error, onChange, value, label, type, required, maxLength, name, calendar } = this.props;
        return (
            <FieldContainer active={value && value.trim().length !== 0}>
                <FieldWrapper focus={this.state.focus} >
                    <FieldLabel>{label}{required && <Req>*</Req>}</FieldLabel>
                    <Input
                        value={value}
                        onChange={onChange}
                        onBlur={this.onBlur}
                        onFocus={this.onFocus}
                        type={type}
                        name={name}
                        aria-describedby={label}
                        aria-label={label}
                        maxLength={maxLength}
                        autoCapitalize="false"
                        autoCorrect="false"
                    />
                </FieldWrapper>
                <Error error={error} active={(error && (error.length > 0)) ? true : false} />
            </FieldContainer>
        )
    }
}





const randomBirthday = {
    day: Math.floor(Math.random() * (30 - 1)) + 1,
    month: Math.floor(Math.random() * (13 - 1)) + 1,
    year: Math.floor(Math.random() * (2005 - 1970)) + 1970,
    
}


export class Calendar extends React.Component {

    state = { focus: false }

    onFocus = () => this.setState({ focus: true })

    onBlur = () => this.setState({ focus: false })

    onChange = e => {
        this.setState({ focus: false })
        this.props.onChange(e)
    }

    render() {
        const { onChange, value, label, name, error } = this.props;
        return (
            <FieldContainer active={true}>
                <FieldWrapper focus={this.state.focus} >
                    <FieldLabel>{label}</FieldLabel>
                    <DatePicker
                        defaultValue={moment(randomBirthday)}
                        style={{ background: 'transparent' }}
                        onChange={this.onChange}
                        placeholder=''
                        onBlur={this.onBlur}
                        onFocus={this.onFocus}
                        className='DayPickerInput'
                    />
                </FieldWrapper>
                <Error error={error} active={(error && (error.length > 0)) ? true : false} />
            </FieldContainer>
        )
    }
}


//export const Input = styled.input`
//	background-color: rgba(255, 255, 255, 0);
//	outline: none;
//    border: none;
//    text-align: center;
//    width: 100%;
//`


//const FieldContainer = styled.div`
//    display: flex;
//    flex-direction: column;
//    position: relative;
//    -webkit-appearance: none;
//    flex-shrink: 0;
//    align-items: center;
//    margin: 0.5rem;
//    text-align: center;
//    ${p => p.active && css`
//        ${Hint} {
//            opacity: 1;
//        }
//        ${FieldWrapper} {  
//	        padding: 1rem 4rem;
//        }
//    `}
//`

//const FieldWrapper = styled.div`
//    position: relative;
//    display: flex;
//	appearance: none;
//	outline: 0;
//	border: 1px solid rgba(255, 255, 255, 0.4);
//	background-color: rgba(255, 255, 255, 0.2);
//	width: 250px;
//	border-radius: 3px;
//	padding: 1rem;
//	margin: 0 auto 10px;
//	text-align: center;
//	font-size: 1.1rem;
//	color: white;
//	transition-duration: 0.25s;
//    &:hover{
//	    background-color: rgba(255, 255, 255, 0.4);
//    }
//    ${p => p.focus && css`
//        &:hover{
//	        background-color: white;
//        }
//        background-color:  white;
//      	width: 300px;
//        ${Hint, Input} {
//            color: #502B5A;
//        }
//    `}
//`

//const Req = styled.div`
//    display: inline;
//    color: red;
//    padding: 0 0.5rem;
//`

//export class Field extends React.Component {

//    state = { focus: false }

//    onFocus = e => this.setState({ focus: true })

//    onBlur = e => {
//        this.setState({ focus: false })
//        this.props.onBlur(e)
//    }
//    render() {
//        const { error, onChange, value, label, required, maxLength, name, type } = this.props;
//        return (
//            <FieldContainer active={value && value.trim().length !== 0}>
//                <FieldWrapper focus={this.state.focus} >
//                    <Label type='user' />
//                    <Input
//                        value={value}
//                        onChange={onChange}
//                        onBlur={this.onBlur}
//                        onFocus={this.onFocus}
//                        type={type}
//                        name={name}
//                        aria-describedby={label}
//                        aria-label={label}
//                        maxLength={maxLength}
//                        autoCapitalize="false"
//                        autoCorrect="false"
//                        placeholder={label}
//                    />
//                </FieldWrapper>
//                <Error error={error} active={(error && (error.length > 0)) ? true : false} />
//            </FieldContainer>
//        )
//    }
//}

//const Hint = styled.div`
//opacity: 0;
//    display: flex;
//    justify-content: center;
//    align-items: center;
//    height: 4rem;
//    width: 4rem;
//position: absolute;
//left: 0;
//top: 0;
//    & > * {
//        font-size: 1.5rem;
//        color: white;
//    }
//`

//const Label = ({ type }) => <Hint>
//    <Icon type={type} />
//</Hint>
﻿import * as React from 'react'
import { Menu, Icon } from 'antd'
import { Link } from 'react-router-dom'


export const Nav = () => <Menu style={style.menu} theme="light" mode="horizontal">
  <Menu.Item >
    <Link to='/'>
        <Icon type="home" />
        Home
    </Link>
  </Menu.Item>
  <Menu.Item>
    <Link to='/new'>
        <Icon type="plus" />
        New contact
    </Link>
  </Menu.Item>
  <Menu.Item>
    <Link to='/login'>
        <Icon type="login" />
        Login
    </Link>
  </Menu.Item>
</Menu>

const style = {
    menu: {
        width: '100%',
        display: 'flex',
        justifyContent: 'space-around',
        lineHeight: '64px'
    }
}
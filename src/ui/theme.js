﻿const css = String.raw

export const color = {
    primary: '#3897f0',

    backgroundLight: '#fafafa',
    backgroundWhite: 'rgba(255,255,255,0.8)',

    text: '#262626',
    textLight: '#999',

    border: '#efefef',
    borderDark: '#e6e6e6',

    success: '#35DC83',
    danger: '#DC3545',
    darkBlue: '#1C1C59',

    darkGrey: '#888898',
    lightGrey: '#DBDBE3'
}

export const font = {
    formElement: '-apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Helvetica, Arial, sans-serif'
}
//background: linear-gradient(to bottom right,#E1E1E7 0%,#9593C7 100%);
export const globalStyles = css`
  html, body {
    font-size: 12px;
    font-family: ${font.formElement};
    -webkit-font-smoothing: antialiased;
    margin: 0;
    padding: 0;
    background: #E1E1E7; 
    background: linear-gradient(to right, #d9a7c7, #fffcdc);
    background: linear-gradient(to bottom right,#E1E1E7 0%,#9593C7 100%);
    background-attachment: fixed
  }
  a {
    text-decoration: none;
    color: #262626 !important;
  }
  a:visited, a:focus, a:hover, a:active {
    text-decoration: none;
    color: #262626!important;
  }
  * {
    box-sizing: border-box;
  }
`
export const variables = {
    fieldHeight: 5,
    fieldUnit: 'rem',
    inputHeight: 3.6,
    inputUnit: 'rem'

}
process.title = 'intouchy:webpack'

const { cpus } = require('os')
const { resolve } = require('path')
const {
  NoEmitOnErrorsPlugin,
  EnvironmentPlugin,
} = require('webpack')
const HappyPack = require('happypack')
// const AssetsPlugin = require('assets-webpack-plugin')
const HtmlPlugin = require('html-webpack-plugin')


const { NODE_ENV } = process.env
const IS_PROD = NODE_ENV === 'production'
const IS_DEV = NODE_ENV === 'development'
const IS_TEST = NODE_ENV === 'test'

const DIST = resolve(__dirname, '..', 'dist')
const SRC = resolve(__dirname, '..', 'src')

const config = {
  context: SRC,
  target: 'web',

  entry: {
    polyfill: [
      'babel-regenerator-runtime',
      'whatwg-fetch',
    ],
    index: ['./index'],
  },

  resolve: {
    extensions: ['.js'],
    modules: [
      'node_modules',
      SRC,
    ],
  },

  output: {
    path: DIST,
  },

  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: [
          'happypack/loader',
        ],
      },
      {
        test: /\.svg$/,
        use: 'react-svg-loader',
      },
    ],
  },

  parallelism: 8,

  plugins: [
    new NoEmitOnErrorsPlugin(),
    new HappyPack({
      threads: cpus().length,
      loaders: ['babel-loader'],
    }),
    new EnvironmentPlugin({
      NODE_ENV: process.env.NODE_ENV || 'development',
    }),

    new HtmlPlugin({
      title: 'InTouch',
      template: resolve(__dirname, '..', 'src', 'index.tpl.html'),
    }),
  ],

  stats: 'errors-only',
}


module.exports = {
  config,

  IS_DEV,
  IS_PROD,
  IS_TEST,

  DIST,
  SRC,
}